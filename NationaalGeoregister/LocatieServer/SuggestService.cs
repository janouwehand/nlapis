﻿using NationaalGeoRegister.LocatieServer.Suggest;
#if NETSTANDARD2_0
using Newtonsoft.Json;
#endif
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace NationaalGeoRegister.LocatieServer
{
  public class SuggestService
  {
    public Container Execute(Query query)
    {
      var url = string.Concat(@"https://geodata.nationaalgeoregister.nl/locatieserver/v3/suggest?q=", HttpUtility.UrlEncode(query.q));

      using (var wc = new WebClient())
      {
        var json_resp = wc.DownloadString(url);
#if NETSTANDARD2_0
        return JsonConvert.DeserializeObject<Container>(json_resp);
#endif

#if NET452
        var ser = new JavaScriptSerializer();
        return ser.Deserialize<Container>(json_resp);
#endif
      }
    }
  }
}
