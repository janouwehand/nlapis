﻿namespace NationaalGeoRegister.LocatieServer.Suggest
{
  public class Response
  {
    public int numFound { get; set; }

    public int start { get; set; }

    public decimal maxScore { get; set; }

    public ResponseItem[] docs { get; set; }
  }
}
