﻿namespace NationaalGeoRegister.LocatieServer.Suggest
{
  public class ResponseItem
  {
    public string type { get; set; }

    public string weergavenaam { get; set; }

    public string id { get; set; }

    public decimal score { get; set; }
  }
}
